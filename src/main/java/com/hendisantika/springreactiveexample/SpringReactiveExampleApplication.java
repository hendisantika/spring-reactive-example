package com.hendisantika.springreactiveexample;

import org.reactivestreams.Publisher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@SpringBootApplication
@RestController
public class SpringReactiveExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringReactiveExampleApplication.class, args);
    }

    //{"name":"hello $name"}
    @GetMapping("/account")
    public Publisher<Account> accountPublisher(@RequestParam String name) {
        return Mono.just(new Account(name)).log()
                .map(account -> new Account("hello " + account.getName()));

    }

    //[{"name":"hello wonwoo"},{"name":"hello kevin"}]
    @GetMapping("/")
    public Publisher<Account> accountsPublisher() {
        return Flux.just(new Account("wonwoo"), new Account("kevin")).log()
                .map(account -> new Account("hello " + account.getName())).log();
    }

    //[2,4,6,8
    @GetMapping("/operator")
    public Publisher<Integer> operatorPublisher() {
        return Flux.just(1, 2, 3, 4, null, 6)
                .map(i -> i * 2);
    }

//    @GetMapping("/operatorError")
//    public Publisher<Integer> operatorErrorPublisher() {
//        return Flux.just(1, 2, 3, 4, null, 6)
//                .map(i -> i * 2).mapError(RuntimeException::new);
//    }

    //[3,4
    @GetMapping("/operatorFilterError")
    public Publisher<Integer> operatorFilterErrorPublisher() {
        return Flux.just(1, 2, 3, 4, null, 6)
                .filter(i -> i > 2);//.mapError(RuntimeException::new);
    }
}


class Account {
    private String name;

    public Account(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        Account account = (Account) o;
        return Objects.equals(name, account.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Account{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
